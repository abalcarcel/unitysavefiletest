﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour {

    public Text path;
    public Text exists;

	// Use this for initialization
	void Start () {
        bool demo;

        Debug.Log(Path.Combine(Application.persistentDataPath, "demo.txt"));
        path.text = Path.Combine(Application.persistentDataPath, "demo.txt");
        demo = System.IO.File.Exists(System.IO.Path.Combine(Application.persistentDataPath, "demo.txt"));
        exists.text = demo.ToString();
        Debug.Log(demo);
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
